import { ToDo } from "../app/interfaces/todo.model";

export const mockToDoList: ToDo[] = [
  {
    userId: 2,
    id: 21,
    title: 'suscipit repellat esse quibusdam voluptatem incidunt',
    completed: false,
  },
  {
    userId: 2,
    id: 23,
    title: 'et itaque necessitatibus maxime molestiae qui quas velit',
    completed: false,
  },
  {
    userId: 2,
    id: 36,
    title: 'excepturi deleniti adipisci voluptatem et neque optio illum ad',
    completed: true,
  },
];
