export interface Address {
  street: string;
  suite: string;
  city: string;
  zipcode: string;
  geo: GeoTag;
}

export interface GeoTag {
  lat: string;
  lng: string;
}
