import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { UserStore } from './store/user-store/user.reducer';
import { error, loadingState } from './store/user-store/user.selectors';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'todo-app';

  constructor(private _store: Store<UserStore>){}

  get isLoading$() {
    return this._store.select(loadingState);
  }

  get error$() {
    return this._store.select(error);
  }
}
