import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { selectUser } from '../../store/user-store/user.actions';
import { mockToDoList, mockUser } from '../../../mocks';

import { UserStore, initialState as userInitialState } from '../../store/user-store/user.reducer';

import { UserListComponent } from './user-list.component';

describe('UserListComponent', () => {
  let component: UserListComponent;
  let fixture: ComponentFixture<UserListComponent>;
  let mockRouter = {
    navigate: jest.fn()
  };

  let store: MockStore<UserStore>;
  const initialState: UserStore = {...userInitialState, users: [mockUser], selectedUser: mockUser, userTodoList: mockToDoList};

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserListComponent ],
      providers: [
        {
          provide: Router, useValue: mockRouter
        }, provideMockStore({ initialState })]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserListComponent);
    component = fixture.componentInstance;
    store = TestBed.inject(MockStore);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate to todo list page on calling viewTodo function ', () => {
    const user = mockUser;
    component.viewTodo(user);

    expect(mockRouter.navigate).toHaveBeenCalledWith(['user', user.id, 'todo']);
  });

  it('should dispatch selectUser action on calling viewTodo function ', () => {
    const storeSpy =  jest.spyOn(store, 'dispatch');
    const user = mockUser;
    component.viewTodo(user);
    fixture.detectChanges();

    expect(storeSpy).toHaveBeenCalledWith(selectUser({selectedUser: user}));
  });
});
