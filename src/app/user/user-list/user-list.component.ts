import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, Subject, takeUntil } from 'rxjs';
import { User } from '../../interfaces/user.model';
import { fetchUsers, selectUser } from '../../store/user-store/user.actions';
import { UserStore } from '../../store/user-store/user.reducer';
import { getUserList } from '../../store/user-store/user.selectors';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent {

  private destroy$ = new Subject();

  constructor(private _router: Router, private _store: Store<UserStore>) {}

  ngOnInit(): void {
    // this._store.dispatch(enableLoadingState());
    this.users$.pipe(takeUntil(this.destroy$)).subscribe(users => {
      if (!users.length) {
        this._store.dispatch(fetchUsers());
      }
    })
  }

  viewTodo(user: User): void {
    this._store.dispatch(selectUser({selectedUser: user}));
    this._router.navigate(['user',user.id , 'todo']);
  }

  get users$(): Observable<User[]> {
    return this._store.select(getUserList);
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
