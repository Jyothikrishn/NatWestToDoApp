import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { UserListComponent } from './user-list/user-list.component';
import { UserTodoListComponent } from './user-todo-list/user-todo-list.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { UserTodoRowComponent } from './user-todo-list/user-todo-row/user-todo-row.component';

@NgModule({
  declarations: [UserListComponent, UserTodoListComponent, UserDetailsComponent, UserTodoRowComponent],
  imports: [
    CommonModule,
    UserRoutingModule
  ],
})
export class UserModule { }
