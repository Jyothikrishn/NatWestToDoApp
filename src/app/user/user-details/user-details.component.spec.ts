import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { mockUser } from '../../../mocks/user.mock';

import { UserDetailsComponent } from './user-details.component';

describe('UserDetailsComponent', () => {
  let component: UserDetailsComponent;
  let fixture: ComponentFixture<UserDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserDetailsComponent);
    component = fixture.componentInstance;
    component.user = mockUser;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should diaplay user details', () => {
    const nameElement = fixture.debugElement.query(By.css('.user-name')).nativeElement.textContent.toString().trim();
    const emailElement = fixture.debugElement.query(By.css('.email')).nativeElement.textContent.toString().trim();
    const phoneNoElement = fixture.debugElement.query(By.css('.phone')).nativeElement.textContent.toString().trim();

    expect(nameElement).toEqual(mockUser.name);
    expect(emailElement).toEqual(mockUser.email);
    expect(phoneNoElement).toEqual(mockUser.phone);
  });

  it('should emit event with user details on calling selectUser function', () => {
    component.onUserSelect.emit = jest.fn();
    component.selectUser();

    expect(component.onUserSelect.emit).toHaveBeenCalledWith(mockUser);

  })
});
