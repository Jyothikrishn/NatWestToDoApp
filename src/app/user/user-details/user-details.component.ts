import { Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '../../interfaces/user.model';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent {

  @Input()
  user?: User;

  @Output()
  onUserSelect = new EventEmitter();

  selectUser() {
    this.onUserSelect.emit(this.user);
  }

}
