import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import {
  UserStore,
  initialState as userInitialState,
} from '../../store/user-store/user.reducer';
import { mockToDoList, mockUser } from '../../../mocks';

import { UserTodoListComponent } from './user-todo-list.component';
import { Router } from '@angular/router';
import { clearUserSelection, clearUsertoDoList } from '../../store/user-store/user.actions';

describe('UserTodoListComponent', () => {
  let component: UserTodoListComponent;
  let fixture: ComponentFixture<UserTodoListComponent>;
  let mockRouter = {
    navigate: jest.fn(),
  };

  let store: MockStore<UserStore>;
  const initialState: UserStore = {
    ...userInitialState,
    users: [mockUser],
    selectedUser: mockUser,
    userTodoList: mockToDoList,
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UserTodoListComponent],
      providers: [
        {
          provide: Router,
          useValue: mockRouter,
        },
        provideMockStore({ initialState })
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(UserTodoListComponent);
    component = fixture.componentInstance;
    store = TestBed.inject(MockStore);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate to users page on calling goBack function ', () => {
    component.goBack();

    expect(mockRouter.navigate).toHaveBeenCalledWith(['/']);
  });


  it('should dispatch clearUserSelection and clearUsertoDoList action on calling goBack function ', () => {
    const storeSpy =  jest.spyOn(store, 'dispatch');
    component.goBack();

    expect(storeSpy).toHaveBeenCalledWith(clearUserSelection());
    expect(storeSpy).toHaveBeenCalledWith(clearUsertoDoList());
  });
});
