import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { mockToDoList, mockUser } from '../../../../mocks';

import { UserTodoRowComponent } from './user-todo-row.component';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { UserStore, initialState as userInitialState } from '../../../store/user-store/user.reducer';
import { toggleToDoStatus } from '../../../store/user-store/user.actions';

describe('UserTodoRowComponent', () => {
  let component: UserTodoRowComponent;
  let fixture: ComponentFixture<UserTodoRowComponent>;

  let store: MockStore<UserStore>;
  const initialState: UserStore = {...userInitialState, users: [mockUser], selectedUser: mockUser, userTodoList: mockToDoList};

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserTodoRowComponent ],
      providers: [
        provideMockStore({ initialState })
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserTodoRowComponent);
    component = fixture.componentInstance;
    component.todo = mockToDoList[0];
    fixture.detectChanges();
    store = TestBed.inject(MockStore);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should todo list user details', () => {
    const titleElement = fixture.debugElement.query(By.css('.title')).nativeElement.textContent.toString().trim();
    const statusElement = fixture.debugElement.query(By.css('.status')).nativeElement.textContent.toString().trim();
    const buttonElement = fixture.debugElement.query(By.css('.toggle-button')).nativeElement.textContent.toString().trim();

    expect(titleElement).toEqual(component.todo.title);
    expect(statusElement).toEqual('Open');
    expect(buttonElement).toEqual('Close');
  });

  it('should dispatch action toggleToDoStatus on calling changeStatus function', () => {
    const storeSpy =  jest.spyOn(store, 'dispatch');
    component.changeStatus();
    fixture.detectChanges();

    expect(storeSpy).toHaveBeenCalledWith(toggleToDoStatus({todo: component.todo}));
  })

  it('should show status label and button label acording to todo item status', () => {
    component.todo = {...component.todo, completed: !component.todo.completed}
    fixture.detectChanges();

    const statusElement = fixture.debugElement.query(By.css('.status')).nativeElement.textContent.toString().trim();
    const buttonElement = fixture.debugElement.query(By.css('.toggle-button')).nativeElement.textContent.toString().trim();

    expect(statusElement).toEqual('Closed');
    expect(buttonElement).toEqual('Open');
  })
});
