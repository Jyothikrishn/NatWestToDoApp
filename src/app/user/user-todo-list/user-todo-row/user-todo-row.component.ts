import { Component, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { ToDo } from '../../../interfaces/todo.model';
import { toggleToDoStatus } from '../../../store/user-store/user.actions';
import { UserStore } from '../../../store/user-store/user.reducer';

@Component({
  selector: '[app-user-todo-row]',
  templateUrl: './user-todo-row.component.html',
  styleUrls: ['./user-todo-row.component.scss']
})
export class UserTodoRowComponent {

  @Input()
  todo: ToDo;

  constructor(private _store: Store<UserStore>) {}

  changeStatus(): void{
    this._store.dispatch(toggleToDoStatus({todo: this.todo}))
  }

  get status(): string {
    return this.todo.completed ? 'Closed' : 'Open'
  }

  get buttonLabel(): string {
    return this.todo.completed ? 'Open' : 'Close'
  }
}
