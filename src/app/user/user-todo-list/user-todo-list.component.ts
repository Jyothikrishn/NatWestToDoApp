import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, Subject, takeUntil } from 'rxjs';
import { ToDo } from '../../interfaces/todo.model';
import { User } from '../../interfaces/user.model';
import { clearUserSelection, clearUsertoDoList, fetchTodoList } from '../../store/user-store/user.actions';
import { UserStore } from '../../store/user-store/user.reducer';
import { getSelectedUser, getToDoList } from '../../store/user-store/user.selectors';

@Component({
  selector: 'app-user-todo-list',
  templateUrl: './user-todo-list.component.html',
  styleUrls: ['./user-todo-list.component.scss']
})
export class UserTodoListComponent implements OnInit, OnDestroy {

  private destroy$ = new Subject();

  constructor(private _router: Router, private _store: Store<UserStore>){}

  get selectedUser$(): Observable<User | undefined> {
    return this._store.select(getSelectedUser);
  }

  get todoList$(): Observable<ToDo[] | undefined> {
    return this._store.select(getToDoList);
  }

  goBack(): void {
    this._store.dispatch(clearUserSelection());
    this._store.dispatch(clearUsertoDoList());
    this._router.navigate(['/'])
  }

  ngOnInit(): void {
    this._store.select(getSelectedUser).pipe(takeUntil(this.destroy$)).subscribe(user => user && this._store.dispatch(fetchTodoList({id: user.id})));
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
