import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { UserListComponent } from "./user-list/user-list.component";
import { UserTodoListComponent } from "./user-todo-list/user-todo-list.component";
import { UserGuard } from "./user.guard";

const routes: Routes = [
  {
    path: '',
    component: UserListComponent
  },
  {
    path: ':id/todo',
    canActivate: [UserGuard],
    component: UserTodoListComponent
  }
];

@NgModule(
  {
    imports: [RouterModule.forChild(routes)],
    exports: [
      RouterModule
    ]
  }
)

export class UserRoutingModule {}
