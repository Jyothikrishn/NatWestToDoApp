import { Injectable } from "@angular/core";
import { CanActivate, Router, UrlTree } from "@angular/router";
import { Store } from "@ngrx/store";
import { map, Observable } from "rxjs";
import { UserStore } from "../store/user-store/user.reducer";
import { getSelectedUser } from "../store/user-store/user.selectors";

@Injectable(
  {
    providedIn: 'root'
  }
)
export class UserGuard implements CanActivate {

  constructor(private _store: Store<UserStore>, private router: Router){}

  canActivate(): Observable<boolean | UrlTree>  {
    return this._store.select(getSelectedUser).pipe(
      map(user => !!user || this.router.createUrlTree(['']))
    )
  }

}
