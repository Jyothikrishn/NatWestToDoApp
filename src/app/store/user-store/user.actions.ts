import { createAction, props } from "@ngrx/store";
import { ToDo } from "src/app/interfaces/todo.model";
import { User } from "src/app/interfaces/user.model";

export const fetchUsers = createAction(
  '[Users] fetch users'
);

export const fetchTodoList = createAction(
  '[Users] fetch user todo list',
  props<{id: number}>()
);

export const loadUsers = createAction(
  '[Users] load users',
  props<{users: User[]}>()
);

export const loadToDoList = createAction(
  '[Users] load user todo list',
  props<{todoList: ToDo[]}>()
);

export const selectUser = createAction(
  '[Users] select user',
  props<{selectedUser: User}>()
);

export const clearUserSelection = createAction(
  '[Users] clear user selection'
);

export const clearUsertoDoList = createAction(
  '[Users] clear user todo list'
);

export const toggleToDoStatus = createAction(
  '[Users] toggle todo status',
  props<{todo: ToDo}>()
);

export const onError = createAction(
  '[Users] onError'
);

// export const disableLoadingState = createAction(
//   '[Users] disable loading state'
// );
