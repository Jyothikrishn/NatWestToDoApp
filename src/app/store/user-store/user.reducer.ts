import { state } from "@angular/animations";
import { createReducer, on } from "@ngrx/store";
import { ToDo } from "src/app/interfaces/todo.model";
import { User } from "src/app/interfaces/user.model";
import * as UserActions from './user.actions';

export interface UserStore {
  users: User[];
  selectedUser?: User | undefined;
  userTodoList: ToDo[];
  isLoading: boolean;
  error: string | undefined
}

export const initialState: UserStore = {
  users: [],
  selectedUser: undefined,
  userTodoList: [],
  isLoading: false,
  error: undefined
}

export const userReducer = createReducer(
  initialState,
  on(UserActions.fetchUsers, (state) => {
    return {
      ...state,
      isLoading: true
    }
  }),
  on(UserActions.fetchTodoList, (state) => {
    return {
      ...state,
      isLoading: true
    }
  }),
  on(UserActions.loadUsers, (state, payload) => {
    return {
      ...state,
      users: payload.users,
      isLoading: false,
      error: undefined
    }
  }),
  on(UserActions.selectUser, (state, payload) => {
    return {
      ...state,
      selectedUser: payload.selectedUser
    }
  }),
  on(UserActions.clearUserSelection, (state) => {
    return {
      ...state,
      selectedUser: undefined
    }
  }),
  on(UserActions.loadToDoList, (state, payload) => {
    return {
      ...state,
      userTodoList: [...payload.todoList],
      isLoading: false,
      error: undefined
    }
  }),
  on(UserActions.clearUsertoDoList, (state) => {
    return {
      ...state,
      userTodoList: []
    }
  }),
  on(UserActions.toggleToDoStatus, (state, payload) => {
    const todoList = [...state.userTodoList];
    const indexOfTodo = todoList.findIndex(todo => +todo.id === +payload.todo.id);
    if (indexOfTodo >= 0) {
      todoList[indexOfTodo] = {...todoList[indexOfTodo], completed: !todoList[indexOfTodo].completed};
    }
    return {
      ...state,
      userTodoList: [...todoList]
    }
  }),
  on(UserActions.onError, (state) => ({...state, error: 'Something went wrong'})),
)
