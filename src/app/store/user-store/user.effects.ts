import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { catchError, map, of, switchMap } from "rxjs";
import { UserService } from "src/app/services/user.service";
import * as UserActions from './user.actions';

@Injectable()
export class UserEffects {
  constructor(private _actions$: Actions, private _userService: UserService){}

  loadUsers = createEffect(() => {
    return this._actions$.pipe(
      ofType(UserActions.fetchUsers),
      switchMap(() => this._userService.fetchUserList().pipe(map(users => UserActions.loadUsers({users: users})), catchError(() => of(UserActions.onError()))))
    );
  });

  loadToDoList = createEffect(() => {
    return this._actions$.pipe(
      ofType(UserActions.fetchTodoList),
      switchMap(user => this._userService.fetchToDoList(user.id).pipe(map(todoList => UserActions.loadToDoList({todoList: todoList})), catchError(() => of(UserActions.onError()))))
    );
  });
}
