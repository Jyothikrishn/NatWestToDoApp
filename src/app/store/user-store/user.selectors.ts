import { createFeatureSelector, createSelector } from "@ngrx/store";
import { UserStore } from "./user.reducer";

export const getUserFeatureSelector = createFeatureSelector<UserStore>('userStore');

export const getUserList = createSelector(
  getUserFeatureSelector,
  (state) => state?.users
);

export const getSelectedUser = createSelector(
  getUserFeatureSelector,
  (state) => state?.selectedUser
);

export const getToDoList = createSelector(
  getUserFeatureSelector,
  (state) => state.userTodoList
);

export const loadingState = createSelector(
  getUserFeatureSelector,
  (state) => state.isLoading
);

export const error = createSelector(
  getUserFeatureSelector,
  (state) => state.error
);
