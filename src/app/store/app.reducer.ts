import { ActionReducerMap } from '@ngrx/store';
import * as userStore from './user-store/user.reducer';

export interface AppState {
  userStore: userStore.UserStore
}

export const appReducer: ActionReducerMap<AppState> = {
  userStore: userStore.userReducer
}
