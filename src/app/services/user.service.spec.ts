import { TestBed } from "@angular/core/testing"
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { UserService } from "./user.service"
import { HttpClient } from "@angular/common/http";
import { mockToDoList, mockUser } from "../../mocks";

describe('UserService', () => {
  let httpClient: HttpClient;
  let userService: UserService;
  let hpptTestingControler: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserService]
    });
    httpClient = TestBed.inject(HttpClient);
    userService = TestBed.inject(UserService);
    hpptTestingControler = TestBed.inject(HttpTestingController)
  });

  it('should call user list api and return user list when fetchUserList() is called', () => {
    userService.fetchUserList().subscribe((res) => {
      expect(res).toEqual([mockUser]);
    });

    const req = hpptTestingControler.expectOne((userService as any)._basrUrl);
    req.flush([mockUser]);
    expect(req.request.url).toEqual((userService as any)._basrUrl);
    expect(req.request.method).toEqual('GET');
  });

  it('should call api to fetch user todo list and return todo list when fetchToDoList() is called', () => {
    userService.fetchToDoList(1).subscribe((res) => {
      expect(res).toEqual(mockToDoList);
    });

    const req = hpptTestingControler.expectOne(`${(userService as any)._basrUrl}/1/todos`);
    req.flush(mockToDoList);
    expect(req.request.url).toEqual(`${(userService as any)._basrUrl}/1/todos`);
    expect(req.request.method).toEqual('GET');
  });
})
