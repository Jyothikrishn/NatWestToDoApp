import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ToDo } from "../interfaces/todo.model";
import { User } from "../interfaces/user.model";

@Injectable({
  providedIn: "root"
})
export class UserService {
  private readonly _basrUrl = "https://jsonplaceholder.typicode.com/users";

  constructor(private _http: HttpClient) {}

  fetchUserList(): Observable<User[]> {
    return this._http.get<User[]>(this._basrUrl);
  }

  fetchToDoList(id: number): Observable<ToDo[]> {
    return this._http.get<ToDo[]>(`${this._basrUrl}/${id}/todos`);
  }
}
